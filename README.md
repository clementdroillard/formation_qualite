# formation_qualite



## Getting started

create .env file :
```
DB_USER=
DB_HOST=
DB_NAME=
DB_PASS=
DB_PORT=
DSN_SENTRY=
```

```
install node (v18.12.1)
install posgresql (v14.6)
npm i 
nodemon server
```

## Tests

```
npm run test
```

## CI

This project uses gitlab CI with file `gitlab-ci.yml`.       
On each commit the CI launch the command `npm run test` on docker image (node 18.12.1 and service postgres 14.6)
