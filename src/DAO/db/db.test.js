require('babel-polyfill');
const db = require('./index');

describe('Tests database', () => {
  test('It should execute query', async () => {
    const { rows } = await db.query('SELECT 1 AS "column"');
    expect(rows).toStrictEqual([{ column: 1 }]);
  });

  test('It should return error on wrong sql request', async () => {
    try {
      await db.query('SELECT zzzzz');
    } catch (error) {
      expect(error.message).toBe('column "zzzzz" does not exist');
    }
  });
});
