CREATE TABLE IF NOT EXISTS city (
   "id" SERIAL PRIMARY KEY,
   "label" VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS trip (
   "id" SERIAL 			  PRIMARY KEY,
   "cityDestinationId"    INT,
   "cityDepartId"         INT,
   "price"                DECIMAL,
   "date"                 TIMESTAMP,
   
   CONSTRAINT FK_trip_city_destination FOREIGN KEY("cityDestinationId") REFERENCES city("id"),
   CONSTRAINT FK_trip_city_depart FOREIGN KEY("cityDepartId") REFERENCES city("id")
);
