const app = require('../../app');
const Trip = require('./trip/Trip');

function initControllers() {
  app.get('/', (req, res) => {
    res.status(200).send({ message: 'Hello World!' });
  });

  app.get('/trip/from/:from/to/:to/date/:date', Trip.getAllTrips);
}

module.exports = {
  initControllers,
};
