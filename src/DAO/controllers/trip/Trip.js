const Sentry = require('@sentry/node');
const db = require('../../db/index');

/**
 * Get all trip for a depart, destination and date
 * @param {object} req
 * @param {object} res
 * @returns {void} return the trips
 */
const getAllTrips = async (req, res) => {
  const getAllTripsQuery = `
    SELECT 
      trip."id"                     AS "tripId",
      trip."price"                  AS "price",
      trip."date"                   AS "date",
      city_depart."id"              AS "cityDepartId",
      city_depart."label"           AS "cityDepartLabel",
      city_destination."id"         AS "cityDestinationId",
      city_destination."label"      AS "cityDestinationLabel"
    FROM trip
    INNER JOIN city city_depart      ON  trip."cityDepartId" = city_depart.id
                                    AND city_depart."label" LIKE CONCAT('%',$1::VARCHAR,'%')
    INNER JOIN city city_destination ON trip."cityDestinationId" = city_destination.id
                                    AND city_destination."label" LIKE CONCAT('%', $2::VARCHAR, '%')
    WHERE trip."date"::DATE = $3
    ORDER BY trip."date"
   `;

  const params = [
    req.params.from,
    req.params.to,
    req.params.date,
  ];
  try {
    const trips = await db.query(getAllTripsQuery, params);

    res.status(200).send({
      from: req.params.from,
      to: req.params.to,
      date: req.params.date,
      trips: trips.rows,
    });
  } catch (error) {
    Sentry.captureException(error);
    res.status(500).send(error);
  }
};
module.exports = {
  getAllTrips,
};
