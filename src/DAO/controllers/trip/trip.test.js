require('babel-polyfill');
const request = require('supertest');
const app = require('../../../index');
const db = require('../../../../tests/db/index');

afterAll(async () => {
  await db.disconnect();
});

describe('Tests trip', () => {
  beforeEach(async () => {
    await db.initDBTest();
  });

  describe('Tests get all trips', () => {
    test('It should return status code 200', (done) => {
      request(app)
        .get('/trip/from/Paris/to/Lille/date/2022-12-12')
        .then((response) => {
          expect(response.statusCode).toBe(200);

          done();
        });
    });

    test('It should params req on json reponse', (done) => {
      request(app)
        .get('/trip/from/Paris/to/Lille/date/2022-12-12')
        .then((response) => {
          expect(response.statusCode).toBe(200);
          expect(response.body.from).toBe('Paris');
          expect(response.body.to).toBe('Lille');
          expect(response.body.date).toBe('2022-12-12');

          done();
        });
    });

    test('It should return all trips from Paris to Lille on 2022-12-12 ordered by date time', (done) => {
      request(app)
        .get('/trip/from/Paris/to/Lille/date/2022-12-12')
        .then((response) => {
          expect(response.statusCode).toBe(200);

          expect(response.body.trips.length).toBe(2);

          expect(response.body.trips[0].tripId).toBe(2);
          expect(response.body.trips[0].price).toBe('102');
          expect(response.body.trips[0].date).toBe('2022-12-12 12:00:00');
          expect(response.body.trips[0].cityDepartId).toBe(2);
          expect(response.body.trips[0].cityDepartLabel).toBe('Paris');
          expect(response.body.trips[0].cityDestinationId).toBe(1);
          expect(response.body.trips[0].cityDestinationLabel).toBe('Lille');

          expect(response.body.trips[1].tripId).toBe(6);
          expect(response.body.trips[1].price).toBe('106');
          expect(response.body.trips[1].date).toBe('2022-12-12 16:00:00');
          expect(response.body.trips[1].cityDepartId).toBe(2);
          expect(response.body.trips[1].cityDepartLabel).toBe('Paris');
          expect(response.body.trips[1].cityDestinationId).toBe(1);
          expect(response.body.trips[1].cityDestinationLabel).toBe('Lille');

          done();
        });
    });

    test('It should return an error on wrong parameter', (done) => {
      request(app)
        .get('/trip/from/Paris/to/Lille/date/ZZZZ')
        .then((response) => {
          expect(response.statusCode).toBe(500);
          expect(response.body.routine).toBe('DateTimeParseError');

          done();
        });
    });
  });
});
