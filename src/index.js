const app = require('./app');
const controllers = require('./DAO/controllers/index');
const sentry = require('../tools/sentry');

const isEnvTest = process.env.NODE_ENV === 'test';

if (!isEnvTest) {
  sentry.initSentry();
}

controllers.initControllers();

if (!isEnvTest) {
  sentry.startSentry();
}

module.exports = app;
