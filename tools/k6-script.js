import http from 'k6/http';
import { sleep } from 'k6';


// command to run :
// k6 run --vus 10 --duration 30s script.js 

export default function () {
  http.get('http://localhost:3000/trip/from/a/to/a/date/2022-12-20');
  sleep(1);
}
