const fs = require('fs');
const path = require('path');
const db = require('../../src/DAO/db/index');

// This will keep node-pg from parsing the value into a Date object
// and give you the raw timestamp string instead.
// It's only for testing timestamp, and for run tests on gilab ci with different timezone.
const { types } = db.pg;
types.setTypeParser(1114, (stringValue) => stringValue);

const initDBTest = async () => {
  const sqlSchema = fs.readFileSync(`${path.resolve(__dirname)}/../../src/DAO/db/init_db_schema.sql`).toString();
  const sqlValues = fs.readFileSync(`${path.resolve(__dirname)}/init_db_values.sql`).toString();
  await db.query(sqlSchema + sqlValues);
};

const disconnect = async () => {
  db.pool.end();
};

module.exports = {
  query: db.query,
  initDBTest,
  disconnect,
};
