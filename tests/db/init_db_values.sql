TRUNCATE TABLE "city" CASCADE;

INSERT INTO "city" 
  ("id", "label"    ) VALUES
  (1   , 'Lille'    ), 
  (2   , 'Paris'    ),
  (3   , 'Nantes'   ),
  (4   , 'Marseille')
;

TRUNCATE TABLE "trip" CASCADE;

INSERT INTO "trip"
  ("id", "cityDepartId", "cityDestinationId", "price", "date"               ) VALUES
  (1   , 1             , 2                  , 101    , '2022-12-12 11:00:00'),
  (2   , 2             , 1                  , 102    , '2022-12-12 12:00:00'),
  (3   , 3             , 2                  , 103    , '2022-12-12 13:00:00'),
  (4   , 4             , 3                  , 104    , '2022-12-12 14:00:00'),
  (5   , 2             , 3                  , 105    , '2022-12-12 15:00:00'),
  (6   , 2             , 1                  , 106    , '2022-12-12 16:00:00'),
  (7   , 2             , 1                  , 107    , '2022-12-13 17:00:00')
;
