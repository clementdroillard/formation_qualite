require('babel-polyfill');
const request = require('supertest');
const app = require('../src/index');

describe('Tests the server', () => {
  test('It should response message', (done) => {
    request(app)
      .get('/')
      .then((response) => {
        expect(response.statusCode).toBe(200);
        expect(response.body.message).toContain('Hello World!');

        done();
      });
  });
});
